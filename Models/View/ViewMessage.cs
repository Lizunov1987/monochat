﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MonoChat.Models.View
{
    public class ViewMessage
    {
        public string UserName { get; set; }
        public string Text { get; set; }
    }
}
