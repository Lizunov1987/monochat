using System;
using System.ComponentModel.DataAnnotations;
using MonoChat.Models.View;

namespace MonoChat.Models
{
    public class MessagesHistoryRequest
    {
        public DateTime? start { get; set; }
        public DateTime? end { get; set; }
    }
}