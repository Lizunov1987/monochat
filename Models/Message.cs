﻿using System;
using System.ComponentModel.DataAnnotations;
using MonoChat.Models.View;

namespace MonoChat.Models
{
    public class Message
    {
        [Key]
        public int id { get;  set; }
        public string UserName { get; set; }
        public string Text { get; set; }
        public DateTime SentTime { get; set; }

    }
}


