﻿using MonoChat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MonoChat.Repository
{
    public interface IMessageRepository
    {
        void Add(Message message);
        void Delete(Message message);
        List<Message> GetMessages(MessagesHistoryRequest request);
    }
}
