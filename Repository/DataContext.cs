﻿using Microsoft.EntityFrameworkCore;
using MonoChat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MonoChat.Repository
{
    public class DataContext: DbContext
    {
        public DataContext(DbContextOptions<DataContext> option) : base(option)
        {
            Database.EnsureCreated();
        }

        public DbSet<Message> Messages { get; set; }

    }
}
