﻿using System;
using System.Collections.Generic;
using System.Linq;
using MonoChat.Models;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
//using System.Data.Entity;

namespace MonoChat.Repository
{
    public class MessageRepository : IMessageRepository
    {
        private DbSet<Message> _messages;
        private DataContext _context;
        public MessageRepository(DataContext context)
        {
            _messages = context.Messages;
            _context = context;
        }
        public void Add(Message message)
        {
            _messages.Add(message);
            _context.SaveChanges();
        }

        public void Delete(Message message)
        {
            _messages.Remove(message);
        }

        public List<Message> GetMessages(MessagesHistoryRequest request)
        {
            IQueryable<Message> result = _messages;
            if (request.start != null)
                result = result.Where(x => x.SentTime >= request.start);
            if (request.end != null)
                result = result.Where(x => x.SentTime <= request.end);
            return result.ToList();
        }


    }
}
