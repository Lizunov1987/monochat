﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MonoChat.Models;
using MonoChat.Models.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using MonoChat.Repository;

namespace MonoChat.Controllers
{
    [ApiController]
    [Route("api")]
    public class MessageController : ControllerBase
    {
        MessageRepository _repo;
        public MessageController(MessageRepository repository)
        {
            _repo = repository;
        }
        [HttpPost]
        [Route("getMessages")]
        public IEnumerable<Message> Get(MessagesHistoryRequest dto)
        {
            return _repo.GetMessages(dto);
        }

        [HttpPost]
        [Route("addMessage")]
        public string Add(ViewMessage message)
        {
            if (string.IsNullOrWhiteSpace(message.UserName))
                throw new Exception("Имя не может быть пустым!");
            var msg = new Message { UserName = message.UserName, Text = message.Text, SentTime = DateTime.Now };
            _repo.Add(msg);
            return  msg.SentTime.ToString("G");
        }
    }
}
